CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `sku` varchar(1024) NOT NULL,
  `price` double NOT NULL,
  `type` int(11) NOT NULL,
  `attributes` varchar(2048) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE products 
  MODIFY COLUMN id INT AUTO_INCREMENT, 
  ADD PRIMARY KEY (id);