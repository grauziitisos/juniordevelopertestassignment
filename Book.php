<?php
namespace Store\Products;


class Book extends Product
{
    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute;
    }

    /**
     * @param string $attribute
     */
    public function setAttribute(string $attribute): void
    {
        if(intval($attribute)!= 0)
            $this->attribute = $attribute;
        else throw new Exception("Weight must be an integer");
    }
}