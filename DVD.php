<?php

namespace Store\Products;

class DVD extends Product
{
    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute;
    }

    /**
     * @param string $attribute
     */
    public function setAttribute(string $attribute): void
    {
        if(intval($attribute)!= 0)
        $this->attribute = $attribute;
        else throw new Exception("Size must be an integer");
    }

}