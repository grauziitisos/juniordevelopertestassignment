<?php

namespace Store\Products;

class Furniture extends Product
{
    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute;
    }

    /**
     * @param string $attribute
     */
    public function setAttribute(string $attribute): void
    {
        $dimensions = explode("x",$attribute);
        if(count($dimensions) == 3 && intval($dimensions[0])!= 0 && intval($dimensions[1])!= 0 && intval($dimensions[2])!= 0)
            $this->attribute = $attribute;
        else throw new Exception("Dimensions must be specified seperated by x");
    }
}