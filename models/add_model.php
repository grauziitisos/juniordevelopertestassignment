<?php


use Store\Products\Book;
use Store\Products\DVD;
use Store\Products\Furniture;

class Add_model extends Model
{

    /**
     * add_model constructor.
     */
    public function __construct()
    {
    }

    public function add_Book(Book $book)
    {
        $sql = "INSERT INTO products (`id`, `sku`, `name`, `price`, `type`, `attributes`) VALUES (NULL, ?,?,?, '1', ?)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$book->getSku(), $book->getName(), $book->getPrice(), $book->getAttribute()]);
    }

    public function add_DVD(DVD $dvd)
    {
        $sql = "INSERT INTO products (`id`, `sku`, `name`, `price`, `type`, `attributes`) VALUES (NULL, ?,?,?, '0', ?)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$dvd->getSku(), $dvd->getName(), $dvd->getPrice(), $dvd->getAttribute()]);
    }

    public function add_Furniture(Furniture $furniture)
    {
        $sql = "INSERT INTO products (`id`, `sku`, `name`, `price`, `type`, `attributes`) VALUES (NULL, ?,?,?, '2', ?)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$furniture->getSku(), $furniture->getName(), $furniture->getPrice(), $furniture->getAttribute()]);
    }
}