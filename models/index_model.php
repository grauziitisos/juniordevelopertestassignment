<?php


use Store\Products\Product;

class Index_model extends Model
{

    /**
     * index_model constructor.
     */
    public function __construct()
    {
    }

    public function getList()
    {
        require("Product.php");
        $data = $this->db->query("SELECT * FROM products");
        $list = array();
        while ($row = $data->fetch()) {
            $product = new Product();
            $product->setName($row['name']);
            $product->setPrice($row['price']);
            $product->setAttribute($row['attributes']);
            $product->setSku($row['sku']);
            $product->setId($row['id']);
            $list[] = $product;
        }
        return $list;
    }

    public function massDelete($ids)
    {
        $idstring = implode("','", $ids);
        $this->db->query("DELETE FROM products WHERE id IN ('" . $idstring . "')");
    }

}