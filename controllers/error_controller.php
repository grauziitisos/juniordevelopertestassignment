<?php


class Error_controller extends Controller
{

    /**
     * Error constructor.
     */
    public function __construct()
    {
        parent::__construct();
        echo "An error occured. Please try again later.";
    }
}