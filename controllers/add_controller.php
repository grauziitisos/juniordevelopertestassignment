<?php

use Store\Products\Book;
use Store\Products\DVD;
use Store\Products\Furniture;

include 'Product.php';
include 'Book.php';
include 'DVD.php';
include 'Furniture.php';

//include 'models/add_model.php';


class Add_controller extends Controller
{
    protected Model $model;

    /**
     * addController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        require("models/add_model.php");
        $this->model = new Add_model();
        $this->model->connect();
        $this->view->render('add');
    }

    private function stripXSS(string $input){
        return str_replace("<", "", $input);
    }

    public function product(): bool
    {
        if (count($_POST) < 1)
            return true;
        var_dump($_POST);
        return true;
    }

    public function book(): bool
    {
        if (isset($_POST['productType'])) {
            if ($_POST['productType'] == '1') {
                $book = new Book();
                $book->setSku($this->stripXSS($_POST["sku"]));
                $book->setName($this->stripXSS($_POST["name"]));
                $book->setPrice(intval($this->stripXSS($_POST["price"])));
                $book->setAttribute($this->stripXSS($_POST["weight"]));
                //for some strange  reason constructor seems to be skipped??
                $this->model->connect();
                $this->model->add_Book($book);
                echo '<script type="text/javascript">window.location = "' . $_SERVER['PHP_SELF'] . '";</script>';
                return true;
            }
        }
        return false;
    }

    public function DVD(): bool
    {
        if (isset($_POST['productType'])) {
            if ($_POST['productType'] == '0') {
                $dvd = new DVD();
                $dvd->setSku($this->stripXSS($_POST["sku"]));
                $dvd->setName($this->stripXSS($_POST["name"]));
                $dvd->setPrice($this->stripXSS(intval($_POST["price"])));
                $dvd->setAttribute($this->stripXSS($_POST["size"]));
                //for some strange  reason constructor seems to be skipped??
                $this->model->connect();
                $this->model->add_DVD($dvd);
                echo '<script type="text/javascript">window.location = "' . $_SERVER['PHP_SELF'] . '";</script>';
                return true;
            }
        }
        return false;
    }

    public function furniture(): bool
    {
        if (isset($_POST['productType'])) {
            if ($_POST['productType'] == '2') {
                $furniture = new Furniture();
                $furniture->setSku($this->stripXSS($_POST["sku"]));
                $furniture->setName($this->stripXSS($_POST["name"]));
                $furniture->setPrice(intval($this->stripXSS($_POST["price"])));
                $furniture->setAttribute($this->stripXSS($_POST["height"]) . 'x' . $this->stripXSS($_POST["width"]) . 'x' . $this->stripXSS($_POST["length"]));
                //for some strange  reason constructor seems to be skipped??
                $this->model->connect();
                $this->model->add_Furniture($furniture);
                echo '<script type="text/javascript">window.location = "' . $_SERVER['PHP_SELF'] . '";</script>';
                return true;
            }
        }
        return false;
    }
}