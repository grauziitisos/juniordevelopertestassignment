<?php


class Index_controller extends Controller
{

    /**
     * indexController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        require "models/index_model.php";
        $this->model = new Index_model();
        $this->model->connect();
        if (isset($_POST["delete"])) {
            if (count($_POST) > 1) {
                $this->massDelete();
            }
            //var_dump($_POST);
            //echo "\n".count($_POST)."<br />";
            //echo "delete pressed";
        }

        $this->view->Products = $this->model->getList();
        $this->view->render('index');
    }

    private function massDelete()
    {
        $array = [];
        foreach ($_POST as  $key => $value) {
            if (intval($key) > 0)
                array_push($array, $key);
        }
        $this->model->massDelete($array);
    }
}