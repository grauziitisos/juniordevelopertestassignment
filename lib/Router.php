<?php


class Router
{

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $url = array_key_exists('url', $_GET) ? $_GET['url'] : null;
        if (isset($url)) {
            $url = rtrim($url, "/");
            $method = explode("-", $url);
            if (count($method) > 1) {
                if (file_exists('controllers/' . $method[0] . '_controller.php')) {
                    require 'controllers/' . $method[0] . '_controller.php';
                    //direct new statement results in class ucfirst not found error..
                    $className = ucfirst(strtolower($method[0].'_controller'));
                    $controller = new $className;
                    $controller->loadModel($method[0]);
                    $controller->{$method[1]}();
                } else {
                    require('controllers/error_controller.php');
                    $controller = new Error_controller();
                }
            } else {
                if (file_exists('controllers/' . $url . '_controller.php')) {
                    require 'controllers/' . $url . '_controller.php';
                    $className = ucfirst($url.'_controller');
                    $controller = new $className;
                    $controller->loadModel($url);
                } else {
                    require('controllers/error_controller.php');
                    $controller = new Error_controller();
                }
            }
        } else {
            require 'controllers/' . 'index_controller.php';
            $controller = new Index_controller();
        }
    }
}